<?php


// -------------------------
//  theme related functions
// -------------------------

/**
* a theme, with ajax functions, to manage 
* the to do list of a node
*/
function theme_todo_items_table_control($nid, $items) {
  // add needed javascript and css
  drupal_add_js('ajax/todo.js');
  //drupal_add_js(drupal_get_path("module", "todo").'/misc/jquery-ui-custom.js');
  drupal_add_css(drupal_get_path("module", "todo").'/misc/todo.css?time='.time());

  // table header
  $header = array(
    t("Tasks"), 
    array("data"=>t("Done"),      "style"=>"width: 15px;"),
    array("data"=>t("Operations"), "style"=>"width: 150px;"),
  );

  // loop through the items to create
  // to do item table
  foreach ($items as $item) {
    $rows[] = array(
      'id'=>_todo_items_table_control_row_id($item),
      'class'=>"todo-table-row",
      'data'=>_todo_items_table_control_row_data($item),
    );
  }

  // table attributes
  $attributes["id"]="todo-table";

  // fieldset contents
  $fieldset_contents = sprintf("<div>%s</div><div>%s</div><div>%s</div>", 
  sprintf('<a href="javascript:todoAddTaskTop(%d)">%s</a>', $nid, t("Add Task")),
  theme_table($header, $rows, $attributes),
  sprintf('<a href="javascript:todoAddTaskBottom(%d)">%s</a>', $nid, t("Add Task")));

  // send back a fieldset with to do table + controls
  return theme_fieldset(array(
    "#type" => "fieldset",
    "#title" => t("To Do"),
    "#collapsible" => TRUE,
    "#collapsed" => empty($items),
    "#value" => $fieldset_contents,
  ));
} // function theme_todo_items_table_control


/**
* theme to indentation of items, to represents depth of item
*/
function theme_todo_indentation($depth=0) {
  if ($depth!==FALSE) return str_repeat('<span class="todo-indentation">&nbsp;</span>', (int) $depth);
  return '%indent%';
} // function theme_todo_indentation


/**
* generate an empty row of current theme
*/
function theme_todo_table_row_item_new() {
  $item->depth=FALSE;
  $item->tid='%tid%';
  $item->title=sprintf("(%s)", t("new item"));
  return theme_todo_table_row_item($item);
} // function theme_todo_table_row_item_new

/**
* theme to a single table row
*/
function theme_todo_table_row_item($item) {
  $row = _todo_items_table_control_row_data($item); 
  $id  = _todo_items_table_control_row_id($item); 
  $fields_html = '';
  foreach ($row as $key=>$field) {
    $fields_html.=sprintf("<td>%s</td>", $field);
  }
  return sprintf('<tr id="%s" class="todo-table-row">%s</tr>', $id, $fields_html);
} // function theme_todo_table_row_item





/**
* helper function to generate todo item table row
*/
function _todo_items_table_control_row_id($item) {
  return "todo-item-row-{$item->tid}";
}

/**
* helper function to generate todo item table row
*/
function _todo_items_table_control_row_data($item) {
  $done_checkbox = ($item->has_child==0) ?
    sprintf('<input type="checkbox" '.
    'name="%s" id="%s" onClick="%s" %s/>',
    "todo_done_tid_{$item->tid}",
    "todo-done-tid-{$item->tid}",
    "javascript:todoToggleTaskDone({$item->tid}, this)", 
    ($item->done == 1) ? ' checked="checked"' : ''): 
    sprintf('<input type="checkbox" '.
    'name="%s" id="%s" %s disabled="disabled"/>',
    "todo_done_tid_{$item->tid}",
    "todo-done-tid-{$item->tid}",
    ($item->done == 1) ? ' checked="checked"' : '');

  return array(
    sprintf('%s<div class="todo-title" id="%s" '.
      'onClick="javascript:textToTextfield(%s, this)">%s</div>', 
      theme_todo_indentation($item->depth),
      "todo-item-title-{$item->tid}", 
      $item->tid, 
      $item->title), 
    $done_checkbox,
    sprintf('<a href="%s">%s</a>', 
      "javascript:todoAddSubtask({$item->tid})", 
      t("Add Subtask")).
    sprintf(' <a href="%s">%s</a>', 
      "javascript:todoDeleteTask({$item->tid})", 
      t("Delete"))
    );
}



