<?php


/**
* these functions are related directly to ajax
*/



/**
* helper function to _todo_menu_ajax_item
*/
function _todo_item_sql_set_clause($array) {
  $clause = "";
  foreach ($array as $key=>$value) {
    $clause .= " {$key}=\"%s\"";
  }
  return array($clause, array_values($array));
}

/**
* helper function to todo_menu_ajax_node
*/
function _todo_menu_ajax_node() {
  $nid = arg(3);
  switch (arg(4)) {
    case "add-task":
      // action: add task
      //$node = node_load($nid);
      $node_count = db_fetch_object(db_query("SELECT COUNT(*) AS count FROM {node} WHERE nid=%d", $nid));
      if ($node_count->count > 0) {
        $tid = todo_item_add_to_root($nid);
        if ($tid !== FALSE) {
          header("Content-Type: text/plain");
          print json_encode(array("result"=>"success", "tid"=>$tid));
        } else {
          header("Content-Type: text/plain");
          print json_encode(array("result"=>"failed", "message"=>"Failed to create new task"));
        }
      } else {
        header("Content-Type: text/plain");
        print json_encode(array("result"=>"failed", "message"=>"Cannot create to in non-exists node."));
      }
      break;
  }
}


/**
* helper function to todo_menu_ajax_item
*/
function _todo_menu_ajax_item() {
  $tid = arg(3);
  if (!empty($tid)) {
    if (is_numeric($tid)) {
      $action = arg(4);
      if (!empty($action)) {
        // access check for current user
        if (_todo_node_access($tid)) drupal_access_denied() && exit();
        
        if ($action=="update") {
          // action: update
          
          // update database
          foreach ($_GET as $key=>$value) {
            if ($key == "done") $value = ($value=="true") ? 1 : 0;
            if (in_array($key, array("title", "done"))) $params[$key] = $value;
          }

          // parameters
          if (!empty($params)) {
            if (!isset($params["done"])) {
              $result = todo_item_update_params($tid, $params);
            } elseif (($item_now = todo_item_load($tid)) && ($item_now->rgt == ($item_now->lft + 1))) {
              $result = todo_item_update_params($tid, $params);
            } else {
              $error_msg = t("Cannot directly set an item done if it has sub-task.");
            }
          } else {
            $error_msg = t("Cannot update a task with no valid parameter.");
          }

          // if success, tell user
          if ($result) {
            header("Content-Type: text/plain");
            $changed_array = todo_ajax_item_check_parents_children_done($tid); // check and mark parent done
            print json_encode(array(
              "result"=>"success",
              "changed"=> (array) $changed_array,
            ));
          } else {
            header("Content-Type: text/plain");
            print json_encode(array("result"=>"failed", "message"=>"$error_msg"));
          }

        } elseif ($action=="add-subtask") {
          // action: add subtask

          // get the parent task item
          $parent_item = todo_item_load($tid);

          if (!empty($parent_item)) {
            // if success, tell user
            if ($new_tid = todo_item_add_into_item($parent_item)) {

              $json_result = json_encode(array(
                "result"   => "success",
                "tid"      => $new_tid, 
                "title"    => "(".t("new item").")",
                "depth"    => $parent_item->depth+1,
                "parentid" => $parent_item->tid,
              ));
              header("Content-Type: text/plain");
              print $json_result;
            } else {
              header("Content-Type: text/plain");
              print json_encode(array("result"=>"failed"));
            }
  
            // check and mark parent done
            todo_ajax_item_check_parents_children_done($next_tid);
          }
        } elseif ($action=="delete") {
          // get the current task
          $item = todo_item_load($tid);
          
          // remove the item
          if (!empty($item)) {
            if ($item->lft == $item->rgt-1) {
  
              // lock the table from writing
              db_query("LOCK TABLE {todo_items} WRITE");

              // delete the item
              $result0 = db_query("DELETE FROM {todo_items} 
                WHERE lft>=%d AND rgt<=%d", $item->lft, $item->rgt);
              
              // remove the room of the old item
              // in 2 steps
  
              // 1. move all right item right border left
              $result1 = db_query("UPDATE {todo_items} SET rgt=rgt-2 
                WHERE rgt>%d AND nid=%d", 
                $item->rgt, $item->nid);
  
              // 2. move all right item left border left
              $result2 = db_query("UPDATE {todo_items} SET lft=lft-2 
                WHERE lft>%d AND nid=%d", 
                $item->lft, $item->nid);

              // allow user to write again
              db_query("UNLOCK TABLES");
              
              // test if it has a single parent item
              $result3 = db_query("SELECT tid FROM {todo_items} 
                WHERE lft=%d AND rgt=%d", $item->lft-1, $item->lft);
              

              if ($result3 && ($single_parent = db_fetch_object($result3))) {
                $single_parent_tid = $single_parent->tid;
              } else {
                $single_parent_tid = 0;
              }
            } else {
              $error_msg = "Cannot delete item contains sub-task.";
            }
          } else {
            $error_msg = "Cannot delete item that does not exists.";
          }

          // if success, tell user
          if ($result0 && $result1 && $result2) {
            header("Content-Type: text/plain");
            print json_encode(array("result"=>"success", "singleParentTid"=>$single_parent_tid));
          } else {
            header("Content-Type: text/plain");
            print json_encode(array("result"=>"failed", "message"=>"$error_msg"));
          }
        }
      }
      exit();
    } else {
      drupal_not_found(); exit;
    }
  }
}

