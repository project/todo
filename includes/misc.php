<?php


/**
* helper to menu item
*/
function todo_menu_js() {
  header("Content-Type: text/plain");
  require_once dirname(__FILE__) . "/js.php";
  exit;
}

/**
* helper to menu item
*/
function todo_menu_ajax_item() {
  require_once dirname(__FILE__)."/ajax.php";
  _todo_menu_ajax_item();
  exit();
}

/**
* helper to menu item
*/
function todo_menu_ajax_node() {
  require_once dirname(__FILE__)."/ajax.php";
  _todo_menu_ajax_node();
  exit();
}




/**
* helper function to get all to do items by nid
*/
function _todo_get_items_with_nid($nid) {

/*
  $result = db_query("SELECT 
  t.*,
  (COUNT(tp.tid) - 1) AS depth,
  IF((t.rgt!=t.lft+1), 1, 0) AS has_child
  FROM {todo_items} AS t,
  {todo_items} AS tp
  WHERE t.nid=%d AND t.lft BETWEEN tp.lft AND tp.rgt
  GROUP BY t.tid
  ORDER BY t.lft;", $nid);
  */

/*
  $result = db_query("SELECT 
  t.*,
  (COUNT(tp.tid) - 1) AS depth,
  IF((t.rgt!=t.lft+1), 1, 0) AS has_child
  FROM 
  {todo_items} AS t
  LEFT JOIN {todo_items} AS tp ON (t.nid=tp.nid AND t.lft BETWEEN tp.lft AND tp.rgt)
  WHERE t.nid=%d
  GROUP BY t.tid
  ORDER BY t.lft;", $nid);
  */

  $result = db_query("SELECT 
  t.*,
  IF((t.rgt!=t.lft+1), 1, 0) AS has_child,
  ( SELECT COUNT(*)-1 FROM {todo_items} AS tp 
    WHERE t.nid=tp.nid AND t.lft BETWEEN tp.lft AND tp.rgt) AS depth
  FROM 
  {todo_items} AS t
  WHERE t.nid=%d
  GROUP BY t.tid
  ORDER BY t.lft;", $nid);

  $items = array();
  while ($item = db_fetch_object($result)) $items[] = $item;
  return $items;
} // function _todo_form_get_items_with_nid

/**
* helper function to check access for to do operation
*/
function _todo_node_access($tid) {
  $result = db_query("SELECT 
  n.*
  FROM 
  {todo_items} ti 
  LEFT JOIN {node} n ON (ti.nid=n.nid)
  WHERE ti.tid=%d", $tid);
  if (($result === FALSE) || 
    !($node = db_fetch_object($result))) return FALSE;
  if (!empty($node) && !node_access("update", $node)) return FALSE;
}


