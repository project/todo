<?php


// ------------------------
//  data control functions
// ------------------------

/**
* With a provided to do id (tid),
* check if all subtask of the parent is done.
* If so, also mark the parent as done
*/
function todo_ajax_item_check_parents_children_done($tid_child) {
  // get information of an item
  $item_child = db_fetch_object(db_query("SELECT tid, lft, rgt FROM {todo_items} WHERE tid=%d", $tid_child));

  // find all parents of an item
  $result = db_query("SELECT * FROM {todo_items} WHERE lft<%d AND rgt>%d AND tid!=%d ORDER BY lft DESC", 
  $item_child->lft, $item_child->rgt, $item_child->tid);
  
  $changed_array = array();
  while ($item_parent = db_fetch_object($result)) {
    if (todo_ajax_item_check_children_done($item_parent)) {
      $changed_array[] = array(
        "tid" => $item_parent->tid,
        "done" => ($item_parent->done == 0) ? 1 : 0,
      );
    }
  }
  return $changed_array;
}

/**
* Check the item's children
* If all of children are done, mark it done.
*/
function todo_ajax_item_check_children_done($item) {
  $result = db_query("SELECT COUNT(*) AS total, SUM(done) AS done 
  FROM {todo_items} WHERE lft>%d AND rgt<%d", $item->lft, $item->rgt);
  if ($children = db_fetch_object($result)) {
    if ($children->total == $children->done) {
      db_query("UPDATE {todo_items} SET done=1 WHERE tid=%d", $item->tid); // set as done
      if ($item->done == 0) return TRUE; // prompted changed
    } else {
      db_query("UPDATE {todo_items} SET done=0 WHERE tid=%d", $item->tid); // set as not done
      if ($item->done == 1) return TRUE; // prompted changed
    }
  }
}

/**
* Load an item
*/
function todo_item_load($tid) {
  return db_fetch_object(db_query("SELECT
  t.*, 
  ( SELECT COUNT(*)-1 FROM {todo_items} AS tp 
    WHERE t.nid=tp.nid AND t.lft BETWEEN tp.lft AND tp.rgt) AS depth
  FROM {todo_items} t
  WHERE t.tid=%d", $tid));
}

/**
* Add a blank item to begining of root
*/
function todo_item_add_to_root($nid) {

  // lock the table from writing
  db_query("LOCK TABLE {todo_items} WRITE");

  // increment id
  $next_tid = db_next_id("{todo_items}");

  // move the current items to make room for the new
  $result1 = db_query("UPDATE {todo_items} SET lft=lft+2, rgt=rgt+2 WHERE nid=%d", 
    $nid);

  // insert the item
  $result2 = db_query("INSERT INTO {todo_items} 
    (tid, nid, title, lft, rgt)
    VALUES (%d, %d, '%s', %d, %d)",
    $next_tid, $nid, "(".t("new item").")", 1, 2
  );

  // allow user to write again
  db_query("UNLOCK TABLES");

  return ($result1 && $result2) ? $next_tid : FALSE;

}

/**
* Add a blank item into an existing item, at the begining
*/
function todo_item_add_into_item($item) {
  // lock the table from writing
  db_query("LOCK TABLE {todo_items} WRITE");

  // increment id
  $next_tid = db_next_id("{todo_items}");

  // move the current items to make room for the new
  // in 3 steps
  
  // 1. move all item on right side of parent right
  $result1 = db_query("UPDATE {todo_items} SET lft=lft+2, rgt=rgt+2
    WHERE lft>%d AND nid=%d", 
    $item->rgt, $item->nid);

  // 2. move parent(s) right border right
  $result2 = db_query("UPDATE {todo_items} SET rgt=rgt+2 
    WHERE lft<=%d AND rgt>=%d AND nid=%d", 
    $item->lft, $item->rgt, $item->nid);

  // 3. move all items inside parent right
  $result3 = db_query("UPDATE {todo_items} SET lft=lft+2, rgt=rgt+2 
    WHERE lft>%d AND rgt<%d AND nid=%d", 
    $item->lft, $item->rgt+2, $item->nid);

  // insert the item
  $result4 = db_query("INSERT INTO {todo_items} 
    (tid, nid, title, lft, rgt)
    VALUES (%d, %d, '%s', %d, %d)",
    $next_tid, $item->nid, "(".t("new item").")", $item->lft+1, $item->lft+2
  );

  // allow user to write again
  db_query("UNLOCK TABLES");

  return ($result1 && $result2 && $result3 && $result4) ? $next_tid : FALSE;
}

/**
* Remove an item
*/
function todo_item_delete($item) {
  // delete the item
  $result0 = db_query("DELETE FROM {todo_items} WHERE tid=%d", $item->tid);
  
  // remove the room of the old item
  // in 2 steps
  
  // 1. move all right item right border left
  $result1 = db_query("UPDATE {todo_items} SET rgt=rgt-2 
    WHERE rgt>%d AND nid=%d", 
    $item->rgt, $item->nid);
  
  // 2. move all right item left border left
  $result2 = db_query("UPDATE {todo_items} SET lft=lft-2 
    WHERE lft>%d AND nid=%d", 
    $item->lft, $item->nid);

  // 3. test if it has a single parent item
  $result3 = db_query("SELECT tid FROM {todo_items} 
  WHERE lft=%d AND rgt=%d", $item->lft-1, $item->lft);
}


function todo_item_update_params($tid, $params) {
  list($set_clause, $sql_params) = _todo_item_sql_set_clause($params);
  $sql_params[] = $tid; // append tid to the end of array
  $result = db_query("UPDATE {todo_items} SET {$set_clause}, updated=UNIX_TIMESTAMP() WHERE tid=%d", $sql_params);
  return ($result != FALSE);
}


