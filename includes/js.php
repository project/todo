<?php
/*
* This file is not used directly.
* Instead, we pass it through Drupal
* We do this so we can get Drupal path 
* function to work for us.
*/
?>

/**
* debug this!!!
*/
/*
$(document).ready(function() {
  $(".todo-table-row").draggable();  // this is not working!!!
});
*/

/**
* This function do these things:
* 1. Announce that we want to add a task
* 2. Query for and pre-occupy a new task id (tid)
* 3. Generate a new row in the task table
*/
function todoAddTaskTop(nid) {
  $.getJSON(
  '<?php print url("ajax/todo/node"); ?>/'+nid+'/add-task',
  {}, todoAddTaskAdd);
}

function todoAddTaskBottom(nid) {
  $.getJSON(
  '<?php print url("ajax/todo/node"); ?>/'+nid+'/add-task',
  {}, todoAddTaskAdd);
}


/**
* This functions do these
* 1. if update successful, add a new empty row to the beginning of todo data
* 2. if update failed, provide error message
*/
function todoAddTaskAdd(data) {
  if (data.result == "success") {
    $("#todo-table tr:first").after(todoThemeItemRow(data.tid, 0)).ready(todoThemeUpdateRows());
  } else {
    alert(data.message);
  }
}

/**
* This function do these things:
* 1. Announce that we want to add a subtask to the task with tid
* 2. Query for and pre-occupy a new task id (tid)
* 3. Generate a new row in the task table
*/
function todoAddSubtask(tid) {
  $.getJSON(
    '<?php print url("ajax/todo/item"); ?>/'+tid+'/add-subtask',
    {}, todoAddSubtaskAdd);
}

/**
* This function do these:
* 1. Tell Drupal that we want to add a new item
* 2. Get the data of the new row
* 3. Create a new row to the table
* 4. Uncheck the parent, then turn the check box into disabled state
* 5. Update table classes for theming
*/
function todoAddSubtaskAdd(data) {
  if (data.result == "success") {
    $("#todo-item-row-"+data.parentid).after(todoThemeItemRow(data.tid, data.depth));
    $("#todo-done-tid-"+data.parentid).attr("disabled", "disabled");
    $("#todo-done-tid-"+data.parentid).attr("checked",  "");
    todoThemeUpdateRows();
  }
}

/**
* This function do these:
* 1. Give the user one last chance to think before delete
* 2. Call todoBseteTaskConfirmed()
*/
function todoDeleteTask(tid) {
  todoDeleteTaskConfirmed(tid);
}

/**
* This function do these:
* 1. Tell Drupal that we want to delete an old item
* 2. Check whether the status is success or not. Return any error message
* 3. Update table classes for theming
* 4. If it is the only child if its parent, remove the disabled attribute
*    from parent check box 
*/
function todoDeleteTaskConfirmed(tid) {
  $.getJSON('<?php print url("ajax/todo/item"); ?>/'+tid+'/delete',
    {}, function (data) {
      if (data.result == "success") {
        $("#todo-item-row-"+tid).remove();
        todoThemeUpdateRows();
        if (data.singleParentTid != 0) {
          $("#todo-done-tid-"+data.singleParentTid).attr("disabled", "");
        }
      } else {
        alert("Item is not deleted. Reason: "+data.message);
      }
    });
}

/**
* This function do these:
* 1. Change a text content into textfield
* 2. Auto focus on the new created textfield (to do)
*/
function textToTextfield(tid, item) {
  id = $(item).attr('id');
  value = $(item).text();
  if ($(item).attr("extra") != "textfield") {
    $(item).html('<input class="todo-textfield" value="'+value+'" '+
    ' onBlur="javascript:textfieldToText(\''+tid+'\', \''+id+'\', this.value)" />'
    ).attr("extra", "textfield").ready(function() {
      /*
      * I wanted to autofocus on the created item
      * but none of these do the job
      */
      //alert("test:" + $("#"+id+" .todo-textfield").attr('class')); // success
      //$("#"+id+" .todo-textfield").focus();                        // failed
      //document.getElementById(id).focus();                         // failed
      //alert($("#"+id+" .todo-textfield").val());                   // failed
    });
  }
}

/**
* This function do these:
* 1. Change the input textfield back to text
* 2. Update the to do item to Drupal
* 3. Get, if any, error and display to user. (to do)
*/
function textfieldToText(tid, id, value) {
  value = value.replace(/&/g, '&amp;');
  value = value.replace(/</g, '&lt;');
  value = value.replace(/>/g, '&gt;');
  $("#"+id).empty().html(value).ready(function() {
      $("#"+id).attr("extra", "text"); // tell others that this field is text now
      newTitle = $("#"+id).html();
      $.getJSON('<?php print url("ajax/todo/item"); ?>/'+tid+'/update', {"title" : newTitle});
  });
}

/**
* This function do these things:
* 1. Update item status
* 2. Get, if any, error and display to user. (to do)
* 3. Update all items from Drupal (to do)
*/
function todoToggleTaskDone(tid, item) {
  value = $(item).attr("checked");
  $.getJSON('<?php print url("ajax/todo/item"); ?>/'+tid+'/update', {"done" : value}, todoTaskDoneUpdate);
}

/**
* This function triggers when todoToggleTaskDone() is called
* This function do these:
* 1. According to data, update checkboxes.
*/
function todoTaskDoneUpdate(data) {
  $.each(data.changed, function(i, item) {
    if (item.done == 1) {
      $("#todo-done-tid-"+item.tid).attr("checked", "checked");
    } else {
      $("#todo-done-tid-"+item.tid).attr("checked", "");
    }
  })
}


//----------------------------
// Messages related functions
//----------------------------

/**
* messages to user
*/
function todoMsgUpdateProcess(tid) {
}

function todoMsgUpdateSuccess(tid) {
}

function todoMsgUpdateFailed(tid) {
}


//-------------------------
// Theme related functions
//-------------------------

/**
* this function provide indentation according to
* the number n
*/
function todoThemeIndent(n){
  var a = [];
  while(a.length < n){
    a.push('<?php print str_replace("'", "\\'", theme_todo_indentation(1)); ?>');
  }
  return a.join('');
}

/**
* this function provide html of an empty row
*/
function todoThemeItemRow(tid, depth) {
  intent = todoThemeIndent(depth);
  template = '<?php print str_replace("'", "\\'", theme_todo_table_row_item_new()); ?>';
  row = template.replace(/%tid%/g, tid);
  row = row.replace(/%indent%/, intent);
  return row;
}

/**
* this function reset table class settings
* to match odd and even class correctly
*/
function todoThemeUpdateRows() {
  $("#todo-table .todo-table-row:odd").removeClass("odd");
  $("#todo-table .todo-table-row:odd").addClass("even");
  $("#todo-table .todo-table-row:even").removeClass("even");
  $("#todo-table .todo-table-row:even").addClass("odd");
}

